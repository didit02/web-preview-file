import { Injectable } from '@angular/core';
import { CanActivate, CanLoad } from '@angular/router';
import { ActivatedRouteSnapshot, Route, Router, RouterStateSnapshot, ActivatedRoute } from '@angular/router';

import { AuthService, TokenService } from '@app/core';
import * as CryptoJS from 'crypto-js';
import { Observable } from 'rxjs/Observable'
@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
    private token: TokenService,
    private route: ActivatedRoute,
  ) {
  }


//   canActivate(route: ActivatedRouteSnapshot,state: RouterStateSnapshot): boolean {
//     if (this.authService.isSignedIn() && localStorage.getItem('currentUser') !== null) {
//       return true;
//     } else {
//       this.router.navigate(['login'], { queryParams: { redirect: state.url } });
//       this.token.removeToken()
//       return false;
//     }
// }

canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
  return new Observable<boolean>(observer => {
          if (this.authService.isSignedIn() && localStorage.getItem('currentUser') !== null) {   
            observer.next(true);          
          }else {
            this.router.navigate(['login'], { queryParams: { redirect: state.url } });
            observer.next(false);
          }
  });
}
}
