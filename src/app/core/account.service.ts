import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { ContentService } from '@app/core/content.service';
import { TokenService } from '@app/core/token.service';
import { AuthService } from '@app/core/auth.service';
import { UserService } from '@app/core/user.service';
import { NotificationService } from './notification.service';
import * as CryptoJS from 'crypto-js';
@Injectable()
export class AccountService {
  lastAccountCheck: Date;
  configData: Array<any>;
  private accountInitStatus: boolean;
  private accountInitialized: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(
    private content: ContentService,
    private token: TokenService,
    private auth: AuthService,
    public user: UserService,
    private notification: NotificationService,
  ) {
    this.accountInitStatus = false;
    if (this.auth.isSignedIn() && !this.accountInitStatus) {
    }
  }

  isAccountInitialized(): boolean {
    return this.accountInitStatus;
  }

  obsAccountInitialized(): Observable<boolean> {
    return this.accountInitialized.asObservable();
  }


  login(data: any): Observable<boolean> {
    const obs = new Observable<any>(observer => {
      this.content.postLogin(data).subscribe(
        res => {
          if (res.token) {
            this.token.setToken(res.token);
            var datapost = {
              user: res.usr_comp_login,
              token: res.token,
              client: res.client_id,
              logo: res.comp_icon,
              levelmenu: res.usr_comp_level,
            }
            localStorage.setItem("currentUser",CryptoJS.AES.encrypt(JSON.stringify(datapost), 'secret').toString());
            this.auth.signIn(data.email);
            observer.next(true);
          } else {
            this.notification.addNotification({
              type: 'danger',
              body: res.message || 'Fail to login'
            });
            this.notification.setNotification(res.message, 'danger', 'modal', 2500);
            observer.next(false);
          }
        },
        err => {
          observer.next(false);
        }
      );
    });
    return obs;
  }
  
}
