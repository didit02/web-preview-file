import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard} from '@app/guard';
import { FrontComponent } from "./front.component";
import { LoginComponent } from "./feature/login/login.component";
import { HomeComponent } from "./feature/home/home.component";
const routes: Routes = [
  {
    path: "",
    component: FrontComponent,
    // canActivate: [AuthGuard],
    children: [
      {
        path: "",
        redirectTo: 'home/url/1' ,
      },
      {
        path: "home/:url/:media",
        component:HomeComponent
      },
    ]
  },
  {
    path: "login",
    component:LoginComponent
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FrontRoutingModule { }
