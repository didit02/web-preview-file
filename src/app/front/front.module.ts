import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from "./shared/components/shared.module";
import { FrontRoutingModule } from './front-routing.module';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';

import { FrontComponent } from "./front.component";
import { AuthGuard} from '@app/guard';
import { LoginComponent } from "./feature/login/login.component";
import { HomeComponent } from "./feature/home/home.component";
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  declarations: [
    FrontComponent,
    LoginComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    FrontRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    PdfViewerModule,
  ],
  providers: [
    AuthGuard
  ]
})
export class FrontModule { }
