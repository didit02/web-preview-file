import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import {AccountService } from '@app/core';
import {FormGroup } from '@angular/forms';
import {NotificationService} from '@app/core';
import { Location } from '@angular/common';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loadingshow:boolean;
  fg: FormGroup = new FormGroup({});
  redirect: string;
  back = false;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private account: AccountService,
    private notifSvc: NotificationService,
    private location: Location,
  ) {
    this.loadingshow = false;
   }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (params['redirect']) this.redirect = params['redirect'].toString();
      else if (params['back']) this.back = true;
    });
  }
  login(userinput, password) {
    const urlRegex: RegExp = /^(?:\w+:)?\/\/([^\s\.]+\.\S{2}|localhost[\:?\d]*)\S*$/;
        this.loadingshow = true;
        let data = {
          username:userinput,
          password: password
        }
        this.account.login(data).subscribe(
          result => {
            if (result) {
              if (this.redirect) {
                if (this.redirect === 'back')
                  if (history.length > 2) this.location.back();
                  else this.router.navigate(['/']);
                else if (urlRegex.test(this.redirect)) window.location.href = this.redirect;
                else this.router.navigate([this.redirect]);
              } else {
                console.log('====');
            }
            }              
          },error => {
            this.loadingshow = false;
          }
        );
  }
  keyuplogin(event, userinput, password) {
    if (event.key === "Enter") {
      this.login(userinput, password);
    }
  }
}
