import { Component,OnInit } from '@angular/core';
import { ActivatedRoute,Router} from "@angular/router";
import { TokenService , ContentService} from "@app/core";
import { DomSanitizer} from '@angular/platform-browser';
import * as $ from "jquery";
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  urlparams:string;
  datagetartikel:any;
  filemediacetak:any;
  filemediatv:any;
  filemediaradio:any;
  constructor(private token : TokenService,private route:ActivatedRoute,
    private content : ContentService,
    private router:Router,
    private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.route.params.subscribe(
      result => {
        if(result){
          this.urlparams = result.url
          if (this.urlparams !== 'url'){
            const datapost = {
              "title": this.urlparams,
              "media_id": result.media
            }
            this.content.postartikelId(datapost).subscribe(
              result => {
                if (result.data){
                  this.datagetartikel = result.data
                  if (result.data.media_type === 'media cetak'){
                    const filesancetak ='/apipdf//pdf_images/'+result.data.file_pdf.substr(0,10).replace(/\-/g,'/')+'/'+result.data.file_pdf
                    // const embed = 'https://drive.google.com/viewerng/viewer?embedded=true&url='+filesancetak+'#toolbar=0&scrollbar=0'
                    // this.filemediacetak = this.sanitizer.bypassSecurityTrustResourceUrl(embed)  
                    this.filemediacetak = filesancetak
                    // this.filemediacetak = 'https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf'
                  }
                  if (result.data.media_type === 'media tv'){
                    const filesanttv ='https://input.digivla.id/media_tv/'+result.data.file_pdf.substr(0,10).replace(/\-/g,'/')+'/'+result.data.file_pdf
                    this.filemediatv = filesanttv
                  }
                  if (result.data.media_type === 'media radio'){
                    const filesanradio ='http://input.digivla.id/media/radio_files/'+result.data.file_pdf.substr(0,10).replace(/\-/g,'/')+'/'+result.data.file_pdf
                    this.filemediaradio = filesanradio
                  }
                }else{
                  this.datagetartikel = ''
                }
              }
            )
          }
        }
      }
    )
  }
  copy(element:any){
    let $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
    alert("Success copy to clipboard");
  }
  logout(){
    this.token.removeToken();
  }
}
